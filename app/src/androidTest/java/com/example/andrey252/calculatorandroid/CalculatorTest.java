package com.example.andrey252.calculatorandroid;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Тесты.
 */
public class CalculatorTest {
    @Test
    public void add() throws Exception {
        assertEquals("Числа не совпадают!", 4, 2 + 2);
    }

    @Test
    public void subtract() throws Exception {
        assertEquals("Числа не совпадают!", 4, 6 - 2);
    }

    @Test
    public void multiply() throws Exception {
        assertEquals("Числа не совпадают!", 4, 2 * 2);
    }

    @Test
    public void divide() throws Exception {
        assertEquals("Числа не совпадают!", 1, 626 / 625);
    }
}