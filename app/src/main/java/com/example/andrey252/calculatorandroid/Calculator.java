package com.example.andrey252.calculatorandroid;

/**
 * Класс {@code Calculator} содержит методы сложения, вычитания, умножения, деления.
 *
 * @author Панфилов Павел.
 */

public class Calculator {
    /**
     * Возвращает сумму своих аргументов, бросая исключение,
     * если результат имеет переполнение {@code double}.
     *
     * @param firstValue  первое слагаемое.
     * @param secondValue второе слагаемое.
     * @return сумма {@code firstValue} + {@code secondValue}.
     * @throws ArithmeticException если результат имеет вещественное переполнение.
     */
    public static double add(double firstValue, double secondValue) {
        double result = firstValue + secondValue;
        if ((Double.MAX_VALUE < result || Double.MIN_VALUE > result) && result != 0) {
            throw new ArithmeticException("Вещественное переполнение!");
        }
        return result;
    }

    /**
     * Возвращает разность своих аргументов, бросая исключение,
     * если результат имеет переполнение {@code double}.
     *
     * @param firstValue  уменьшаемое.
     * @param secondValue вычитаемое.
     * @return разность {@code firstValue} - {@code secondValue}.
     * @throws ArithmeticException если результат имеет вещественное переполнение.
     */
    public static double subtract(double firstValue, double secondValue) {
        double result = firstValue - secondValue;
        if ((Double.MAX_VALUE < result || Double.MIN_VALUE > result) && result != 0) {
            throw new ArithmeticException("Вещественное переполнение!");
        }
        return result;
    }

    /**
     * Возвращает произведение своих аргументов, бросая исключение,
     * если результат имеет переполнение {@code double}.
     *
     * @param firstValue  первый множитель.
     * @param secondValue второй множитель.
     * @return произведение {@code firstValue} * {@code secondValue}.
     * @throws ArithmeticException если результат имеет вещественное переполнение.
     */
    public static double multiply(double firstValue, double secondValue) {
        double result = firstValue * secondValue;
        if ((Double.MAX_VALUE < result || Double.MIN_VALUE > result) && result != 0) {
            throw new ArithmeticException("Вещественное переполнение!");
        }
        return result;
    }

    /**
     * Возвращает частное своих аргументов {@code double}, бросая исключение,
     * если присутствует деление на ноль {@code secondValue = 0}.
     *
     * @param firstValue  делимое.
     * @param secondValue делитель.
     * @return частное {@code firstValue} / {@code secondValue}.
     * @throws ArithmeticException если присутствует деление на ноль.
     */
    public static double divide(double firstValue, double secondValue) {
        if (secondValue == 0) {
            throw new ArithmeticException("Деление на ноль!");
        }
        return firstValue / secondValue;
    }
}