/**
 * @author Рыжкин Андрей 14ИТ18к
 * @version 1.1
 * @since 23.11.2017
 */
package com.example.andrey252.calculatorandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public final String RESULT = "result";

    private EditText firstOperandObj;
    private EditText secondOperandObj;

    private Button resetButton;
    private Button addButton;
    private Button subtractButton;
    private Button multiplyButton;
    private Button divideButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstOperandObj = (EditText) findViewById(R.id.first_operand);
        secondOperandObj = (EditText) findViewById(R.id.second_operand);
        resetButton = (Button) findViewById(R.id.reset_button);
        addButton = (Button) findViewById(R.id.add_button);
        subtractButton = (Button) findViewById(R.id.subtract_button);
        multiplyButton = (Button) findViewById(R.id.multiply_button);
        divideButton = (Button) findViewById(R.id.divide_button);

        resetButton.setOnClickListener(this);
        addButton.setOnClickListener(this);
        subtractButton.setOnClickListener(this);
        multiplyButton.setOnClickListener(this);
        divideButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.reset_button) {
            resetOperations();
            return;
        }
        if (validationOperand(firstOperandObj) && validationOperand(secondOperandObj)) {
            startResultActivity(selectOperation(view));
        }
    }

    /**
     * Метод очищает значение двух полей
     */
    private void resetOperations() {
        firstOperandObj.setText("");
        secondOperandObj.setText("");
    }

    /**
     * Метод выбирает арифметическую операцию, в зависимости какую кнопку нажали
     * (сложение, вычитание,умножение, деление), возращая результат вычислений.
     *
     * @param view вьюшка
     * @return возращает результат вычислений в виде строки.
     */
    private String selectOperation(View view) {
        double firstOperand = Double.parseDouble(firstOperandObj.getText().toString());
        double secondOperand = Double.parseDouble(secondOperandObj.getText().toString());
        String resultOperation = null;
        switch (view.getId()) {
            case R.id.add_button:
                resultOperation = String.valueOf((Calculator.add(firstOperand, secondOperand)));
                break;
            case R.id.subtract_button:
                resultOperation = String.valueOf((Calculator.subtract(firstOperand, secondOperand)));
                break;
            case R.id.multiply_button:
                resultOperation = String.valueOf((Calculator.multiply(firstOperand, secondOperand)));
                break;
            case R.id.divide_button:
                try {
                    resultOperation = String.valueOf((Calculator.divide(firstOperand, secondOperand)));
                } catch (ArithmeticException e) {
                    Toast.makeText(this, "Деление на ноль", Toast.LENGTH_LONG).show();
                }
                break;
        }
        return resultOperation;
    }

    /**
     * Метод выполняет переход на другую ResultActivity, сохраняя результат вычислений.
     *
     * @param result результат вычислений
     */
    private void startResultActivity(String result) {
        if (result != null) {
            Intent intent = new Intent(MainActivity.this, ResultActivity.class);
            intent.putExtra(RESULT, result);
            startActivity(intent);
        }
    }

    /**
     * Метод проверяет корректность введенного поля.
     *
     * @param operandObj объект поля
     * @return возвращает {@code true} если данные в поле введены корректно, иначе {@code false}
     */
    private boolean validationOperand(EditText operandObj) {
        String operand = operandObj.getText().toString();
        if (operand.length() == 0) {
            operandObj.setError("Поле не может быть пустым!");
            return false;
        }
        if (operand.charAt(operand.length() - 1) == '.') {
            operandObj.setError("Поле не может заканчиваться \".\"");
            return false;
        }
        if (operand.charAt(0) == '-' && operand.length() == 1) {
            operandObj.setError("Некорректное значение!");
            return false;
        }
        return true;
    }
}